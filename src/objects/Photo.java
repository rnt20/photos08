package objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Photo is an object representing a photo.
 * Stores its tags, caption, time the photo was taken, and file path.
 *
 * @author Rahul Trivedi
 * @author Derek Wong
 */
public class Photo implements Serializable {
    public ArrayList<Tag> tags;
    public String caption;
    public Calendar dateTime;
    public String filePath;
    public int numAlbums = 0;

    /**
     * updateDetails updates a photo's metadata.
     *
     * @param tags     photo's new tags
     * @param caption  photo's new caption
     * @param dateTime photo's new date of capture
     */
    public void updateDetails(ArrayList<Tag> tags, String caption, Calendar dateTime) {
        if (tags != null)
            this.tags = tags;
        if (caption != null) {
            if (caption.equals("")) {
                this.caption = null;
            } else {
                this.caption = caption;
            }
        }
        if (dateTime != null)
            this.dateTime = dateTime;
    }

}
