package objects;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Temp is a debugging application for the program.
 */
public class Temp {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        HashMap<String, Photo> hashmap = new HashMap<>();
        ArrayList<User> users = new ArrayList<>();
        User user = new User("stock");
        user.addAlbum("stock");
        Album album = user.albums.get(0);
        album.addPicture(hashmap, "data/stock1.jpg");
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(new Tag("person", "bobby"));
        tags.add(new Tag("location", "home"));
        String caption = "nerd kid";
        Calendar c = Calendar.getInstance();
        File file = new File("data/stock1.jpg");
        long lastModified = file.lastModified();
        c.setTimeInMillis(lastModified);
        c.set(Calendar.MILLISECOND,0);
        Photo p = hashmap.get("data/stock1.jpg");
        p.filePath ="data/stock1.jpg";
        album.addPicture(hashmap, "data/stock2.jpg");
        album.addPicture(hashmap, "data/stock3.jpg");
        album.addPicture(hashmap, "data/stock4.jpg");
        album.addPicture(hashmap, "data/stock5.jpg");
        users.add(user);
        user.photos = hashmap;

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"));
        oos.writeObject(users);
        /*
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("users.txt"));
        ArrayList<User> users = (ArrayList<User>)ois.readObject();
        System.out.println(users);
        for(User a: users){
            System.out.println(a.username);
        }
        */




    }

}
