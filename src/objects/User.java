package objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * User is an object representing a user account.
 * Stores its username, albums, and HashMap that maps photo locations to their Photo object.
 *
 * @author Rahul Trivedi
 * @author Derek Wong
 */

public class User implements Serializable {
    public String username;
    public ArrayList<Album> albums;
    public HashMap<String, Photo> photos;
    public ArrayList<String> tagTypes;
    /**
     * User constructor.
     * @param username the username for the account.
     */
    public User(String username){
        this.username = username;
        albums = new ArrayList<>();
        photos = new HashMap<String, Photo>();
        tagTypes = new ArrayList<>();
        tagTypes.add("person");
        tagTypes.add("location");
    }

    /**
     * addAlbum adds a new album to the user's list. Returns true is successful, false if album name already exists.
     *
     * @param name album's name
     * @return boolean value of whether or not album was added
     */
    public boolean addAlbum(String name){
        for(Album a : albums){
            if(a.name.equals(name)){
                return false;
            }
        }
        albums.add(new Album(name));
        return true;
    }
    /**
     * addAlbum renames a album in the user's list. Returns true is successful, false if album name already exists.
     *
     * @param name album's name
     * @param album the album being changed
     * @return boolean value of whether or not album name was changed
     */
    public boolean renameAlbum(Album album, String name){
        for(Album a : albums){
            if(a.name.equals(name)){
                return false;
            }
        }
        album.name = name;
        return true;
    }
}
