package objects;

import javafx.scene.control.Alert;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Album is an object representing an album of photos.
 * The album stores its name as well as a list of all its photos' file paths.
 * @author Rahul Trivedi
 * @author Derek Wong
 */

public class Album implements Serializable {
    public ArrayList<String> locations;
    public String name;
    /**
     * Album constructor.
     * @param name the name of the album.
     */
    public Album(String name){
        this.name = name;
        locations = new ArrayList<>();
    }
    /**
     * addPicture adds a picture to current album and appropriately assigns the time the photo was taken.
     * Does not allow duplicate pictures.
     * Returns true if successful add, false otherwise.
     * @param allPhotos a HashMap of all the current user's photos.
     * @param location the file path of the photo being added to the album.
     * @return boolean value with success of adding photo
     */

    public boolean addPicture(HashMap<String, Photo> allPhotos, String location){
        for(String photo: locations){
            if(photo.equals(location)){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialog");
                alert.setHeaderText("Error adding photo");
                alert.setContentText("Photo already exists in album");

                alert.showAndWait();
                return false;
            }
        }
        if(!allPhotos.containsKey(location)){
            Photo photo = new Photo();
            File file = new File(location);
            long lastModified = file.lastModified();
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(lastModified);
            c.set(Calendar.MILLISECOND,0);
            photo.dateTime = c;
            photo.filePath = location;
            photo.tags = new ArrayList<>();
            allPhotos.put(location, photo);
        }
        allPhotos.get(location).numAlbums++;
        locations.add(location);
        return true;
    }
}
