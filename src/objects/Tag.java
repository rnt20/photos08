package objects;

import java.io.Serializable;
/**
 * Tag is an object representing a photo's tag.
 * Stores its name and value.
 *
 * @author Derek Wong
 */
public class Tag implements Serializable {
    public String name;
    public String value;
    /**
     * Tag default constructor.
     */
    public Tag() {
        this.name = "";
        this.value = "";
    }
    /**
     * Tag's two-arg constructor.
     * @param name tag's name
     * @param value tag's value
     */
    public Tag(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
