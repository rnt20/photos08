package app;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.controllers.*;
/**
 * Photos is the main class for the application
 * @author Rahul Trivedi
 * @author Derek Wong
 */
public class Photos extends Application {
    /**
     * start sets up the scenes of the program and switches to the login scene after setup
     * @throws Exception if exception in login controller's display
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        //Login Controller Settings
        FXMLLoader loginLoader = new FXMLLoader();
        loginLoader.setLocation(getClass().getResource("/view/scenes/login.fxml"));
        AnchorPane loginRoot = loginLoader.load();
        LoginController loginController = loginLoader.getController();
        Scene login = new Scene(loginRoot, 530, 380);

        //Admin Controller Settings
        FXMLLoader adminLoader = new FXMLLoader();
        adminLoader.setLocation(getClass().getResource("/view/scenes/admin.fxml"));
        AnchorPane adminRoot = adminLoader.load();
        AdminController adminController = adminLoader.getController();
        Scene admin = new Scene(adminRoot, 530, 380);
        loginController.setAdminScene(admin, adminController);
        adminController.setLogin(login, loginController);

        //User Controller Settings
        FXMLLoader userLoader = new FXMLLoader();
        userLoader.setLocation(getClass().getResource("/view/scenes/user.fxml"));
        AnchorPane userRoot = userLoader.load();
        UserController userController = userLoader.getController();
        Scene user = new Scene(userRoot, 530, 380);
        loginController.setUserScene(user, userController);

        //Album Controller Settings
        FXMLLoader albumLoader = new FXMLLoader();
        albumLoader.setLocation(getClass().getResource("/view/scenes/album.fxml"));
        AnchorPane albumRoot = albumLoader.load();
        AlbumController albumController = albumLoader.getController();
        Scene album = new Scene(albumRoot, 530, 380);
        albumController.setController(userController, user);

        //Search Controller Settings
        FXMLLoader searchLoader = new FXMLLoader();
        searchLoader.setLocation(getClass().getResource("/view/scenes/search.fxml"));
        AnchorPane searchRoot = searchLoader.load();
        SearchController searchController = searchLoader.getController();
        Scene search = new Scene(searchRoot, 530, 380);
        userController.setControllers(login, loginController, album, albumController, search, searchController);
        searchController.setController(userController, user);


        loginController.display(primaryStage);
        primaryStage.setScene(login);
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
