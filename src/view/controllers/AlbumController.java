package view.controllers;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import objects.Photo;
import objects.Tag;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.stage.Stage;
import objects.*;

import java.io.*;

/**
 * AlbumController is the controller for the album scene.
 *
 * @author Derek Wong
 */
public class AlbumController {
    UserController userController;
    Scene user;

    Stage mainStage;
    User current;
    ArrayList<User> userList;
    HashMap<String, Photo> photos;
    Album currAlbum;
    private ObservableList<Photo> photosList;
    ArrayList<String> tagTypes;
    @FXML
    Text albumName;
    @FXML
    ListView<Photo> photosListView;
    @FXML
    ImageView pictureDisplay;
    @FXML
    Text details;

    /**
     * setController sets up the user controller and scene.
     *
     * @param userController the controller for the user scene.
     * @param user           the user scene.
     */
    public void setController(UserController userController, Scene user) {
        this.userController = userController;
        this.user = user;
    }

    /**
     * changeSelection updates the scene's details and image based on the currently selected item in the list.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void changeSelection() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            details.setText("Caption: \nDatetime: \nTags: ");
            pictureDisplay.setImage(null);
        } else {
            String caption = photosList.get(index).caption;
            if (caption == null)
                caption = "No caption";
            String dateTime = "";
            Calendar c = photosList.get(index).dateTime;
            if (c == null)
                dateTime = "No datetime";
            else {
                Date date = c.getTime();
                DateFormat dateFormat = new SimpleDateFormat("MM dd yyyy HH:mm");
                dateTime = dateFormat.format(date);
            }
            ArrayList<Tag> tagList = photosList.get(index).tags;
            String tags = "";
            if (tagList == null) {
                tags = "No tags";
            } else {
                for (Tag t : tagList) {
                    tags += t.name + ": " + t.value + ", ";
                    tags += " ";
                }
            }
            details.setText("Caption: " + caption + "\nDatetime: " + dateTime + "\nTags: " + tags);
            InputStream stream = new FileInputStream(currAlbum.locations.get(index));
            Image image = new Image(stream);
            pictureDisplay.setImage(image);

        }
    }

    /**
     * display renders the scene based on the current album's photos.
     *
     * @param mainStage the stage which we are using.
     * @param photos    a HashMap of all the user's photos.
     * @param current   the current user.
     * @param currAlbum the current album.
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void display(Stage mainStage, HashMap<String, Photo> photos, User current, Album currAlbum) throws FileNotFoundException {
        this.current = current;
        this.mainStage = mainStage;
        this.photos = photos;
        this.currAlbum = currAlbum;
        this.tagTypes = current.tagTypes;
        albumName.setText(currAlbum.name);
        mainStage.setMinWidth(854);
        mainStage.setMinHeight(480);
        photosList = FXCollections.observableArrayList();
        for (String location : currAlbum.locations) {
            photosList.add(photos.get(location));
        }
        photosListView.setItems(photosList);
        changeSelection();
        photosListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldval, newVal) -> {
            try {
                changeSelection();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        photosListView.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    InputStream stream = null;
                    try {
                        stream = new FileInputStream(photo.filePath);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Image image = new Image(stream);
                    imageView.setImage(image);
                    imageView.setFitWidth(50);
                    imageView.setFitHeight(50);
                    String caption = photo.caption;
                    if (caption == null) caption = "no caption";
                    setText(caption);
                    setGraphic(imageView);
                }
            }
        });
    }

    /**
     * back changes the scene to user when the back button is pressed.
     */
    public void back() {
        userController.update();
        mainStage.setScene(user);
    }

    /**
     * add prompts user to select a photo from the file system to be added to the album
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void add() throws FileNotFoundException {
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files", "*.BMP", "*.GIF", "*.JPEG", "*.JPG", "*.PNG");
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Select photo");
        File file = fileChooser.showOpenDialog(mainStage);
        if (file != null)
            currAlbum.addPicture(photos, file.getAbsolutePath());
        refresh();

    }

    /**
     * previous selects the previous photo from the one currently selected in the album.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void previous() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        if (index == 0) {
            index = photosList.size() - 1;
        } else {
            index--;
        }
        photosListView.getSelectionModel().select(index);
        changeSelection();


    }

    /**
     * next selects the next photo in the album when the next button is pressed.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void next() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        if (index == photosList.size() - 1) {
            index = 0;
        } else {
            index++;
        }
        photosListView.getSelectionModel().select(index);
        changeSelection();
    }

    /**
     * edit allows the user to change a photo's caption.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void edit() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }

        TextInputDialog dialog = new TextInputDialog(photosList.get(index).caption);
        dialog.setContentText("Caption: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            photosList.get(index).updateDetails(null, result.get(), null);

        }
        changeSelection();
        refresh();

    }

    /**
     * remove allows the user to remove a photo from the album.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void remove() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Are you sure you want to delete this image?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            String location = currAlbum.locations.get(index);
            photos.get(location).numAlbums--;
            if(photos.get(location).numAlbums == 0){
                photos.remove(location);
            }
            currAlbum.locations.remove(index);
            photosList.remove(index);
        }
        refresh();
    }

    /**
     * copy allows user to copy a photo to another album.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void copy() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        ArrayList<String> choices = new ArrayList<>();
        for (Album a : current.albums) {
            choices.add(a.name);
        }
        ChoiceDialog<String> dialog = new ChoiceDialog<>(current.albums.get(0).name, choices);
        dialog.setTitle("Choice Dialog");
        dialog.setHeaderText("Copy a photo to an album");
        dialog.setContentText("Choose an album");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Photo p = photosList.get(index);
            for (int i = 0; i < current.albums.size(); i++) {
                if (current.albums.get(i).name.equals(result.get())) {
                    current.albums.get(i).addPicture(photos, p.filePath);
                    break;
                }
            }
        }
        refresh();
    }

    /**
     * move allows user to move a photo to a different album.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void move() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        ArrayList<String> choices = new ArrayList<>();
        for (Album a : current.albums) {
            choices.add(a.name);
        }
        ChoiceDialog<String> dialog = new ChoiceDialog<>(current.albums.get(0).name, choices);
        dialog.setTitle("Choice Dialog");
        dialog.setHeaderText("Move a photo to an album");
        dialog.setContentText("Choose an album");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Photo p = photosList.get(index);
            for (int i = 0; i < current.albums.size(); i++) {
                if (current.albums.get(i).name.equals(result.get())) {
                    if (current.albums.get(i).addPicture(photos, p.filePath)) {
                        p.numAlbums--;
                        currAlbum.locations.remove(index);
                    }
                    break;
                }
            }

        }
        refresh();
    }

    /**
     * createTag allows user to create a new type of tag that will show up in their add tag dialog.
     */
    public void createTag() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Text Input Dialog");
        dialog.setHeaderText("Create a new tag");
        dialog.setContentText("Please enter the tag's type");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            if (!tagTypes.contains(result.get())) {
                tagTypes.add(result.get());
            }
        }

    }

    /**
     * addTag allows user to add a tag to a photo.
     * Does not allow duplicate tags on the same photo.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void addTag() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        ArrayList<String> choices = tagTypes;
        ChoiceDialog<String> dialog = new ChoiceDialog<>(choices.get(0), choices);
        dialog.setTitle("Choice Dialog");
        dialog.setHeaderText("Add a tag");
        dialog.setContentText("Choose your tag:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String tagName = result.get();
            TextInputDialog newDialog = new TextInputDialog();
            newDialog.setTitle("Text Input Dialog");
            newDialog.setHeaderText("Add a tag");
            newDialog.setContentText("Please enter the tag's value");
            result = newDialog.showAndWait();
            if (result.isPresent()) {
                String tagValue = result.get();
                boolean found = false;
                if (photosList.get(index).tags != null) {
                    for (Tag t : photosList.get(index).tags) {
                        if (tagName.equals(t.name) && tagValue.equals(t.value)) {
                            found = true;
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error Dialog");
                            alert.setHeaderText("Error adding tag");
                            alert.setContentText("You may not have a duplicate tag for a photo.");
                            alert.showAndWait();
                            break;
                        }
                    }
                }
                if (!found) {
                    photosList.get(index).tags.add(new Tag(tagName, tagValue));
                    refresh();
                }
            }
        }

    }

    /**
     * removeTag allows user to remove tag from photo.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void removeTag() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Photo Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        ArrayList<Tag> tags = photosList.get(index).tags;
        ArrayList<String> tagList = new ArrayList<>();
        for (Tag t : tags) {
            tagList.add(t.name + ": " + t.value);
        }
        ChoiceDialog<String> dialog = new ChoiceDialog<>(tagList.get(0), tagList);
        dialog.setTitle("Choice Dialog");
        dialog.setHeaderText("Add a tag");
        dialog.setContentText("Choose your tag:");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            int num = dialog.getItems().indexOf(result.get());
            tags.remove(num);
            refresh();
        }


    }

    /**
     * refresh renders display again.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void refresh() throws FileNotFoundException {
        display(mainStage, photos, current, currAlbum);
    }

}
