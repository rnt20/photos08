package view.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import objects.*;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Optional;

/**
 * UserController is the controller for the admin scene.
 *
 * @author Rahul Trivedi
 * @author Derek Wong
 */

public class UserController {
    LoginController loginController;
    Scene login;

    AlbumController albumController;
    Scene album;

    SearchController searchController;
    Scene search;

    Stage mainStage;
    User current;
    ArrayList<User> userList;
    HashMap<String, Photo> photos;
    private ObservableList<String> albumList;

    @FXML
    ListView<String> list;
    @FXML
    Text details;

    /**
     * setControllers sets up the user controller and scene
     *
     * @param login the admin scene
     * @param loginController the controller for the user scene
     * @param album the search scene
     * @param albumController the controller for the search scene
     * @param search the search scene
     * @param searchController the controller for the search scene
     */
    public void setControllers(Scene login, LoginController loginController, Scene album, AlbumController albumController, Scene search, SearchController searchController){
        this.login = login;
        this.loginController = loginController;
        this.album = album;
        this.albumController = albumController;
        this.search = search;
        this.searchController = searchController;
    }

    /**
     * update changes the display and list with new changes from the search or album screen
     *
     */
    public void update(){
        if(albumList.size() != current.albums.size()){
            albumList.add(current.albums.get(current.albums.size()-1).name );
        }
        int index = list.getSelectionModel().getSelectedIndex();
        if(index == -1){
            details.setText("Name: " + "\nSize: " + "\nEarliest Photo: " + "\nLatest Photo: ");
            return;
        }
        Album currAlbum = current.albums.get(index);
        int size = currAlbum.locations.size();
        if(size == 0){
            details.setText("Name: " + currAlbum.name + "\nSize: " + size + "\nEarliest Photo: " + "\nLatest Photo: ");
            return;
        }
        ArrayList<String> locations = currAlbum.locations;
        Calendar earliest = photos.get(locations.get(0)).dateTime;
        Calendar latest = photos.get(locations.get(0)).dateTime;
        for(int i = 1; i < locations.size(); i++){
            Calendar curr = photos.get(locations.get(i)).dateTime;
            if(curr.before(earliest)){
                earliest = curr;
            }
            else if(curr.after(latest)){
                latest = curr;
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("MM dd yyyy HH:mm");
        details.setText("Name: " + currAlbum.name + "\nSize: " + size + "\nEarliest Photo: " + dateFormat.format(earliest.getTime()) + "\nLatest Photo: " + dateFormat.format(latest.getTime()));
    }

    /**
     * addAlbum allows the user to add a album to the account with an input dialog. Gives an alert if the album already exists.
     *
     */
    public void addAlbum(){
        TextInputDialog dialog = new TextInputDialog();
        dialog.initOwner(mainStage);
        dialog.setHeaderText("Enter Album Name");
        dialog.setContentText("Album Name: ");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            if(result.get().trim().equals("")){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "Album can't be blank";
                alert.setContentText(content);
                alert.show();
            }
            else if (!current.addAlbum(result.get())) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "Album already exists";
                alert.setContentText(content);
                alert.show();
            } else {
                albumList.add(result.get());
            }
        }
    }

    /**
     * renameAlbum allows the user to rename a album to the account with an input dialog. Gives an alert if the album already exists.
     *
     */
    public void renameAlbum(){
        int index = list.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Album Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        TextInputDialog dialog = new TextInputDialog(albumList.get(index));
        dialog.initOwner(mainStage);
        dialog.setHeaderText("Enter Album Name");
        dialog.setContentText("Album Name: ");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()) {
            if(result.get().trim().equals("")){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "Album can't be blank";
                alert.setContentText(content);
                alert.show();
            }
            else if (!current.renameAlbum(current.albums.get(index), result.get())) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "Album already exists";
                alert.setContentText(content);
                alert.show();
            } else {
                albumList.set(index, result.get());
            }
        }
    }

    /**
     * deleteAlbum allows the user to delete a album to the account with an input dialog. Gives an alert if no album is selected.
     *
     */
    public void deleteAlbum(){
        int index = list.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Album Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Are you sure you want to delete this album?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            Album a = current.albums.get(index);
            for(String location: a.locations){
                photos.get(location).numAlbums--;
                if(photos.get(location).numAlbums == 0){
                    photos.remove(location);
                }
            }
            current.albums.remove(index);
            albumList.remove(index);
        }
    }

    /**
     * openAlbum opens the selected album and swtiches the album scene. If no album is selected, gives an alert.
     * @throws FileNotFoundException if exception in album controller's display
     */
    public void openAlbum() throws FileNotFoundException {
        //change scene to album
        int index = list.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No Album Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        Album currAlbum = current.albums.get(index);
        albumController.display(mainStage, photos, current, currAlbum);
        mainStage.setScene(album);
    }
    /**
     * search switches to the search scene
     * @throws FileNotFoundException if exception in search controller's display
     */
    public void search() throws FileNotFoundException {
        searchController.display(mainStage, photos, current);
        mainStage.setScene(search);
    }

    /**
     * logout moves the admin back to the login scene after saving any changes.
     *
     * @throws Exception if users.dat is not found
     */
    public void logout() throws Exception {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Are you sure you want to logout?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"));
            oos.writeObject(userList);
            loginController.display(mainStage);
            mainStage.setScene(login);
        }
    }

    /**
     * display renders the scene based on the current user's albums.
     *
     * @param mainStage the stage which we are using.
     * @param userList a list of all the users in the system.
     * @param user the current user.
     */
    public void display(Stage mainStage, ArrayList<User> userList, User user){
        this.mainStage = mainStage;
        this.userList= userList;
        albumList = FXCollections.observableArrayList();
        current = user;
        photos = current.photos;
        for(Album a: user.albums){
            albumList.add(a.name);
        }
        list.setItems(albumList);
        list.getSelectionModel().select(0);
        update();
        list.getSelectionModel().selectedIndexProperty().addListener((obs, oldval, newVal) -> update());
    }
}
