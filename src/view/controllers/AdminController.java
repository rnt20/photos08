package view.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import objects.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

/**
 * AdminController is the controller for the admin scene.
 *
 * @author Rahul Trivedi
 * @author Derek Wong
 */

public class AdminController {
    private ObservableList<String> usernameList;
    private Scene login;
    private LoginController loginController;
    Stage mainStage;
    ArrayList<User> userList;
    @FXML
    ListView<String> list;

    /**
     * remove allows the admin to remove a user from the system with confirmation. Gives an alert if no user is selected.
     *
     */
    public void remove(){
        int index = list.getSelectionModel().getSelectedIndex();
        if(index == -1){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(mainStage);
            alert.setTitle("Error");
            String content = "No User Selected";
            alert.setContentText(content);
            alert.show();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Are you sure you want to delete " + usernameList.get(index));
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            userList.remove(index);
            usernameList.remove(index);
        }
    }
    /**
     * setAdminScene sets up the admin controller and scene.
     *
     * @param login the admin scene.
     * @param loginController the controller for the user scene.
     */
    public void setLogin(Scene login, LoginController loginController){
        this.login = login;
        this.loginController = loginController;
    }
    /**
     * add allows the admin to add a user to the system with an input dialog. Gives an alert if the user already exists.
     *
     */
    public void add() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.initOwner(mainStage);
        dialog.setHeaderText("Enter Username");
        dialog.setTitle("New User");
        dialog.setContentText("Username: ");
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()){
            if(result.get().trim().equals("")){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "User can't be blank";
                alert.setContentText(content);
                alert.show();
            }
            else if(result.get().equals("admin")){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "User can't be admin";
                alert.setContentText(content);
                alert.show();
            }
            else if(usernameList.contains(result.get())){
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(mainStage);
                alert.setTitle("Error");
                String content = "Username already exists";
                alert.setContentText(content);
                alert.show();
            } else {
                userList.add(new User(result.get()));
                usernameList.add(result.get());
            }
        }
    }


    /**
     * logout moves the admin back to the login scene after saving any changes.
     *
     * @throws Exception if error in opening or writing to users.dat
     */
    public void logout() throws Exception {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Are you sure you want to logout?");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK) {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"));
            oos.writeObject(userList);
            mainStage.setScene(login);
        }
    }

    /**
     * readUserList takes the usernames from the user list for display in the listview
     *
     */
    public void readUserList(){
        for(User u: userList){
            usernameList.add(u.username);
        }
    }
    /**
     * display sets up the admin scene and the listview for displaying usernames
     * @param mainStage the stage for the application
     * @param userList list of users in the application
     */
    public void display(Stage mainStage, ArrayList<User> userList) {
        this.mainStage = mainStage;
        usernameList = FXCollections.observableArrayList();
        this.userList = userList;
        readUserList();
        list.setItems(usernameList);
        list.getSelectionModel().select(0);
    }
}
