package view.controllers;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import objects.*;

import java.io.*;
import java.util.ArrayList;

/**
 * LoginController is the controller for the login scene.
 *
 * @author Rahul Trivedi
 * @author Derek Wong
 */
public class LoginController {
    private Scene admin;
    private Scene user;
    private AdminController adminController;
    private UserController userController;
    ArrayList<User> usernameList;
    Stage mainStage;

    @FXML
    Button submit;
    @FXML
    TextField username;
    /**
     * readUserList reads in the user data from the last session and loads it into usernameList
     */
    public void readUserList(){
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("users.dat"));
            usernameList = (ArrayList<User>) ois.readObject();
        } catch(Exception e){
            usernameList = new ArrayList<>();
        }
    }
    /**
     * setAdminScene sets up the admin controller and scene.
     *
     * @param admin the admin scene.
     * @param adminController the controller for the user scene.
     */
    public void setAdminScene(Scene admin, AdminController adminController){
        this.admin = admin;
        this.adminController = adminController;
    }
    /**
     * setUserScene sets up the user controller and scene.
     *
     * @param user the user scene.
     * @param userController the controller for the user scene.
     *
     */
    public void setUserScene(Scene user, UserController userController){
        this.user = user;
        this.userController = userController;
    }

    /**
     * switchScene checks for login input and then switches the scene to admin scene or user scene if correct.
     * Displays an alert for incorrect input
     */
    public void switchScene(){
        String input = username.getText();
        if(input.equals("admin")){
            adminController.display(mainStage, usernameList);
            mainStage.setScene(admin);
            return;
        }
        for(User u: usernameList){
            if(u.username.equals(input)){
                userController.display(mainStage, usernameList, u);
                mainStage.setScene(user);
                return;
            }
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(mainStage);
        alert.setTitle("Error");
        String content = "Incorrect Username";
        alert.setContentText(content);
        alert.show();

    }

    /**
     * onClose saves the updated changes and is called if a user presses the X
     * @throws IOException if error in opening or writing to users.dat
     */
    public void onClose() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.dat"));
        oos.writeObject(usernameList);
    }

    /**
     * display sets up the login scene
     * @param mainStage the stage for the application
     */
    public void display(Stage mainStage){
        usernameList = new ArrayList<>();
        readUserList();
        this.mainStage = mainStage;
        mainStage.setOnCloseRequest(windowEvent -> {
            try {
                onClose();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
