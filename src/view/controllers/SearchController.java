package view.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import objects.Album;
import objects.Photo;
import objects.Tag;
import objects.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * SearchController is the controller for the search scene.
 *
 * @author Derek Wong
 */
public class SearchController {
    UserController userController;
    Scene user;

    User current;
    Stage mainStage;
    HashMap<String, Photo> photos;
    private ObservableList<Photo> photosList;

    @FXML
    ListView<Photo> photosListView;
    @FXML
    ImageView pictureDisplay;
    @FXML
    Text details;
    @FXML
    TextField searchBar;


    /**
     * setController sets up the user scene and controller.
     *
     * @param userController the user controller.
     * @param user           the user scene.
     */
    public void setController(UserController userController, Scene user) {
        this.userController = userController;
        this.user = user;
    }

    /**
     * changeSelection changes the scene based on the currently selected item in the list.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void changeSelection() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if (index == -1) {
            details.setText("Caption: \nDatetime: \nTags: ");
            pictureDisplay.setImage(null);
        } else {
            String caption = photosList.get(index).caption;
            if (caption == null)
                caption = "No caption";
            String dateTime = "";
            Calendar c = photosList.get(index).dateTime;
            if (c == null)
                dateTime = "No datetime";
            else {
                Date date = c.getTime();
                DateFormat dateFormat = new SimpleDateFormat("MM dd yyyy HH:mm");
                dateTime = dateFormat.format(date);
            }
            ArrayList<Tag> tagList = photosList.get(index).tags;
            String tags = "";
            if (tagList == null) {
                tags = "No tags";
            } else {
                for (Tag t : tagList) {
                    tags += t.name + ": " + t.value + ", ";
                    tags += " ";
                }
            }
            details.setText("Caption: " + caption + "\nDatetime: " + dateTime + "\nTags: " + tags);
            InputStream stream = new FileInputStream(photosList.get(index).filePath);
            Image image = new Image(stream);
            pictureDisplay.setImage(image);

        }
    }

    /**
     * display renders the scene for the first time based on the user's current photos.
     *
     * @param mainStage the main stage.
     * @param photos    the user's photos.
     * @param current   the current user.
     * @throws FileNotFoundException if image path could not be opened.
     */

    public void display(Stage mainStage, HashMap<String, Photo> photos, User current) throws FileNotFoundException {
        this.mainStage = mainStage;
        this.photos = photos;
        this.current = current;
        mainStage.setMinWidth(854);
        mainStage.setMinHeight(480);
        photosList = FXCollections.observableArrayList();
        Set<String> allLocations = photos.keySet();
        for (String location : allLocations) {
            photosList.add(photos.get(location));
        }
        photosListView.setItems(photosList);
        photosListView.getSelectionModel().select(0);
        changeSelection();
        photosListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldval, newVal) -> {
            try {
                changeSelection();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        photosListView.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    InputStream stream = null;
                    try {
                        stream = new FileInputStream(photo.filePath);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Image image = new Image(stream);
                    imageView.setImage(image);
                    imageView.setFitWidth(50);
                    imageView.setFitHeight(50);
                    String caption = photo.caption;
                    if (caption == null) caption = "no caption";
                    setText(caption);
                    setGraphic(imageView);
                }
            }
        });
    }

    /**
     * back sends the user back to the user scene.
     */

    public void back() {
        userController.update();
        mainStage.setScene(user);
    }

    /**
     * previous changes list's selection to the previous.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */
    public void previous() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if (index == 0) {
            index = photosList.size() - 1;
        } else {
            index--;
        }
        photosListView.getSelectionModel().select(index);
        changeSelection();


    }

    /**
     * next changes list's selection to the next.
     *
     * @throws FileNotFoundException if image path could not be opened.
     */

    public void next() throws FileNotFoundException {
        int index = photosListView.getSelectionModel().getSelectedIndex();
        if (index == photosList.size() - 1) {
            index = 0;
        } else {
            index++;
        }
        photosListView.getSelectionModel().select(index);
        changeSelection();
    }

    /**
     * search searches the user's photos for photos that match the searchquery.
     * Accepts a search for a tag, two tags with AND or OR, or a date range formatted as MM/dd/yyyy:MM/dd/yyyy.
     * Renders all photos fitting the criteria in the list.
     *
     * @throws FileNotFoundException if image path could not be opened.
     * @throws ParseException        if formatter cannot parse string.
     */

    public void search() throws FileNotFoundException, ParseException {
        photosList = FXCollections.observableArrayList();
        Set<String> allLocations = photos.keySet();
        for (String location : allLocations) {
            photosList.add(photos.get(location));
        }
        String searchQuery = searchBar.getText();
        if (searchQuery.equals("")) {
            display(mainStage, photos, current);
            return;
        }

        ObservableList<Photo> newPhotosList = FXCollections.observableArrayList();
        if (searchQuery.contains(":")) {
            StringTokenizer t = new StringTokenizer(searchQuery, ":");
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date startDate = formatter.parse(t.nextToken());
            Date endDate = formatter.parse(t.nextToken());
            endDate.setTime(endDate.getTime() + (60 * 60 * 24 * 1000 - 1));
            for (Photo p : photosList) {
                if (p.dateTime.getTimeInMillis() >= startDate.getTime() && p.dateTime.getTimeInMillis() <= endDate.getTime()) {
                    newPhotosList.add(p);
                }
            }
        } else if (searchQuery.contains("=")) {
            if (searchQuery.contains(" AND ")) {
                StringTokenizer t = new StringTokenizer(searchQuery, " AND ");
                String tag1String = t.nextToken();
                String tag2String = t.nextToken();
                t = new StringTokenizer(tag1String, "=");
                Tag tag1 = new Tag();
                tag1.name = t.nextToken();
                tag1.value = t.nextToken();
                t = new StringTokenizer(tag2String, "=");
                Tag tag2 = new Tag();
                tag2.name = t.nextToken();
                tag2.value = t.nextToken();

                for (Photo p : photosList) {
                    boolean tag1match = false;
                    boolean tag2match = false;
                    if (p.tags != null) {
                        for (Tag tag : p.tags) {
                            if (tag1.name.equals(tag.name) && tag1.value.equals(tag.value)) {
                                tag1match = true;

                            } else if (tag2.name.equals(tag.name) && tag2.value.equals(tag.value)) {
                                tag2match = true;
                            }

                        }

                    }
                    if (tag1match && tag2match) {
                        newPhotosList.add(p);
                    }
                }
            } else if (searchQuery.contains(" OR ")) {
                StringTokenizer t = new StringTokenizer(searchQuery, " OR ");
                String tag1String = t.nextToken();
                String tag2String = t.nextToken();
                t = new StringTokenizer(tag1String, "=");
                Tag tag1 = new Tag();
                tag1.name = t.nextToken();
                tag1.value = t.nextToken();
                t = new StringTokenizer(tag2String, "=");
                Tag tag2 = new Tag();
                tag2.name = t.nextToken();
                tag2.value = t.nextToken();

                for (Photo p : photosList) {
                    boolean tag1match = false;
                    boolean tag2match = false;
                    if (p.tags != null) {
                        for (Tag tag : p.tags) {
                            if (tag1.name.equals(tag.name) && tag1.value.equals(tag.value)) {
                                tag1match = true;

                            } else if (tag2.name.equals(tag.name) && tag2.value.equals(tag.value)) {
                                tag2match = true;
                            }
                        }
                    }
                    if (tag1match || tag2match) {
                        newPhotosList.add(p);
                    }
                }

            } else {
                StringTokenizer token = new StringTokenizer(searchQuery, "=");
                String tagName = token.nextToken();
                String tagValue = token.nextToken();
                Tag tag = new Tag();
                tag.name = tagName;
                tag.value = tagValue;
                for (Photo p : photosList) {
                    if (p.tags != null) {
                        for (Tag t : p.tags) {
                            if (t.name.equals(tag.name) && t.value.equals(tag.value)) {
                                newPhotosList.add(p);
                                break;
                            }
                        }

                    }
                }
            }
        }
        photosList = newPhotosList;
        photosListView.setItems(photosList);

        photosListView.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    InputStream stream = null;
                    try {
                        stream = new FileInputStream(photo.filePath);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Image image = new Image(stream);
                    imageView.setImage(image);
                    imageView.setFitWidth(50);
                    imageView.setFitHeight(50);
                    String caption = photo.caption;
                    if (caption == null) caption = "no caption";
                    setText(caption);
                    setGraphic(imageView);
                }
            }
        });
        photosListView.getSelectionModel().select(0);
        photosListView.getSelectionModel().selectedIndexProperty().addListener((obs, oldval, newVal) -> {
            try {
                changeSelection();
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        });

    }

    /**
     * addToAlbum adds all photos currently in the rendered list to a new album.
     * User is prompted for the new album's name.
     * The album may not be a duplicate in the user's albums.
     */
    public void addToAlbum() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setHeaderText("Create new album");
        dialog.setContentText("Album Name: ");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String newAlbumName = result.get();
            for (Album a : current.albums) {
                if (newAlbumName.equals(a.name)) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialog");
                    alert.setHeaderText("Error adding album");
                    alert.setContentText("You may not have a duplicate album.");
                    alert.showAndWait();
                    return;
                }
            }
            Album newAlbum = new Album(newAlbumName);
            newAlbum.locations = new ArrayList<>();
            for (Photo p : photosList) {
                newAlbum.locations.add(p.filePath);
                p.numAlbums++;
            }
            current.albums.add(newAlbum);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Successfully created album");
            alert.setContentText("Your album has been created.");
            alert.showAndWait();
        }

    }


}
